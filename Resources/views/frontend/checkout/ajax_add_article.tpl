{extends file='parent:frontend/checkout/ajax_add_article.tpl'}

{block name='checkout_ajax_add_actions'}
    <div class="modal--actions">

        {* Contiune shopping *}
        {block name='checkout_ajax_add_actions_continue'}
            <a href="{$detailLink}" data-modal-close="true" title="{s name='AjaxAddLinkBack'}{/s}" class="link--back btn is--secondary is--left is--icon-left is--large">
                {s name='AjaxAddLinkBack'}{/s} <i class="icon--arrow-left"></i>
            </a>
        {/block}

        {* Fast checkout *}
        {block name='checkout_ajax_add_actions_fast_checkout'}
            <a href="{if {config name=always_select_payment}}{url controller='checkout' action='shippingPayment'}
                {else}{url controller='checkout' action='confirm'}{/if}" class="link--checkout btn is--primary right is--icon-right is--large"
               title="{s name='AjaxCartLinkConfirm' namespace="frontend/checkout/ajax_cart"}{/s}">
                <i class="icon--arrow-right"></i>
                {s name='AjaxCartLinkConfirm' namespace="frontend/checkout/ajax_cart"}{/s}
            </a>
        {/block}

        {* Forward to the checkout *}
        {block name='checkout_ajax_add_actions_checkout'}
            <a href="{url action=cart}" title="{s name='AjaxAddLinkCart'}{/s}" class="link--confirm btn right is--large">
                <span class="hr-icon-cart hr-icon--space-right"></span>
                {s name='AjaxAddLinkCart'}{/s} {*<i class="icon--arrow-right"></i>*}
            </a>
        {/block}

    </div>
{/block}

{block name='checkout_ajax_add_actions_continue'}
    <a href="{$detailLink}" data-modal-close="true" title="{s name='AjaxAddLinkBack'}{/s}" class="link--back btn left is--secondary is--left is--icon-left is--large">
        {s name='AjaxAddLinkBack'}{/s} <i class="icon--arrow-left"></i>
    </a>
{/block}


{block name='checkout_ajax_add_actions_checkout'}
    <a href="{url action=cart}" title="{s name='AjaxAddLinkCart'}{/s}" class="link--confirm btn left is--large">
        <span class="hr-icon-cart hr-icon--space-right"></span>
        {s name='AjaxAddLinkCart'}{/s} {*<i class="icon--arrow-right"></i>*}
    </a>
    <div class="right modal_paypal_button{if !$PaypalLocale || $PaypalLocale == 'de_DE'}_de{/if}">
        <a href="{url controller=payment_paypal action=express forceSecure}">
            {if !$PaypalLocale || $PaypalLocale == 'de_DE'}
                <img src="{link file="frontend/_resources/images/paypal_express_large.png"}"
                     alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}"
                     title="{s name='PaypalButtonLinkTitleText' namespace='frontend/_includes_paypal/express'}{/s}">
            {else}
                <img src="https://www.paypal.com/{$PaypalLocale}/i/btn/btn_xpressCheckout.gif">
            {/if}
        </a>
    </div>
{/block}

