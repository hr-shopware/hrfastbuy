{extends file='parent:frontend/checkout/cart.tpl'}

{block name="frontend_checkout_cart_table_actions"}
    {$smarty.block.parent}
    {block name="frontend_checkout_paypal_payment_express_top"}
        <div class="paypal-express--container">
            {if $sUserLoggedIn}
                <div class="paypal-express">

                    {* PayPal express delimiter *}
                    <span class="paypal-express--delimiter">
                        {s name='PaypalButtonDelimiter' namespace='frontend/_includes_paypal/express'}{/s}
                    </span>

                    {* PayPal express button *}
                    <a href="{url controller=payment_paypal action=express forceSecure}"
                       title="{s name='PaypalButtonLinkTitleText' namespace='frontend/_includes_paypal/express'}{/s}"
                       class="paypal-express--btn">
                        {if !$PaypalLocale || $PaypalLocale == 'de_DE'}
                            <img srcset="{link file='frontend/_public/src/img/paypal-button-express-de.png'}, {link file='frontend/_public/src/img/paypal-button-express-de-2x.png'} 2x"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}">
                        {elseif $PaypalLocale|strpos:"en" !== false}
                            <img src="{link file='frontend/_public/src/img/paypal-button-express-en.png'}"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}">
                        {else}
                            <img src="https://www.paypal.com/{$PaypalLocale}/i/btn/btn_xpressCheckout.gif"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}">
                        {/if}
                    </a>
                </div>
            {/if}
        </div>
    {/block}
{/block}

{block name="frontend_checkout_cart_table_actions_bottom"}
    {$smarty.block.parent}
    {block name="frontend_checkout_paypal_payment_express_bottom"}
        <div class="paypal-express--container">
            {if $sUserLoggedIn}
                <div class="paypal-express">

                    {* PayPal express delimiter *}
                    <span class="paypal-express--delimiter">
                        {s name='PaypalButtonDelimiter' namespace='frontend/_includes_paypal/express'}{/s}
                    </span>

                    {* PayPal express button *}
                    <a href="{url controller=payment_paypal action=express forceSecure}"
                       title="{s name='PaypalButtonLinkTitleText' namespace='frontend/_includes_paypal/express'}{/s}"
                       class="paypal-express--btn">
                        {if !$PaypalLocale || $PaypalLocale == 'de_DE'}
                            <img srcset="{link file='frontend/_public/src/img/paypal-button-express-de.png'}, {link file='frontend/_public/src/img/paypal-button-express-de-2x.png'} 2x"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}xyz">
                        {elseif $PaypalLocale|strpos:"en" !== false}
                            <img src="{link file='frontend/_public/src/img/paypal-button-express-en.png'}"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}">
                        {else}
                            <img src="https://www.paypal.com/{$PaypalLocale}/i/btn/btn_xpressCheckout.gif"
                                 alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}">
                        {/if}
                    </a>
                </div>
            {/if}
        </div>
    {/block}
{/block}
