{extends file="parent:frontend/detail/buy.tpl"}

{block name="frontend_detail_buy_button"}
    {if $sArticle.sConfigurator && !$activeConfiguratorSelection}
        <button id="variant" type="submit" class="buybox--button block btn is--disabled is--large" disabled="disabled" aria-disabled="true" name="{s name="DetailBuyActionAddName"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
            <span class="hr-icon-cart-add hr-icon--space-right"></span>{s name="DetailBuyActionAdd"}{/s}
        </button>
    {else}
        <button id="default" type= "submit" class="buybox--button block btn is--primary is--center is--large" name="{s name="DetailBuyActionAddName"}{/s}"{if $buy_box_display} style="{$buy_box_display}"{/if}>
            <span class="hr-icon-cart-add hr-icon--space-right"></span>{s name="DetailBuyActionAdd"}{/s}
        </button>
    {/if}


    <div class="paypal-button">
        <button id="detail-paypal" type="submit" class="right" style="border-style:none; padding:0;">
            {if !$PaypalLocale || $PaypalLocale == 'de_DE'}
                <img src="{link file="frontend/_resources/images/paypal_express_large.png"}"
                     alt="{s name='PaypalButtonAltText' namespace='frontend/_includes_paypal/express'}{/s}"
                     title="{s name='PaypalButtonLinkTitleText' namespace='frontend/_includes_paypal/express'}{/s}">
            {else}
                <img src="https://www.paypal.com/{$PaypalLocale}/i/btn/btn_xpressCheckout.gif">
            {/if}
        </button>
    </div>

{/block}
