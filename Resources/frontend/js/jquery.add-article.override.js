$.overridePlugin('swAddArticle', {


    /**
     * Gets called when the element was triggered by the given event name.
     * Serializes the plugin element {@link $el} and sends it to the given url.
     * When the ajax request was successful, the {@link initModalSlider} will be called.
     *
     * @public
     * @event sendSerializedForm
     * @param {jQuery.Event} event
     */
    sendSerializedForm: function (event) {
        event.preventDefault();

        var me = this,
            opts = me.opts,
            $el = me.$el,
            ajaxData = $el.serialize();

        ajaxData += '&isXHR=1';

        if (opts.showModal) {
            $.loadingIndicator.open({
                'openOverlay': true
            });
        }

        $.publish('plugin/swAddArticle/onBeforeAddArticle', [me, ajaxData]);

        var buttonId = $el.find('button.clicked').prop('id');
        var buttonName = $el.find('button.clicked').prop('name');

        $.ajax({
            data: ajaxData,
            dataType: 'html',
            method: 'POST',
            url: opts.addArticleUrl,
            success: function (result) {
                $.publish('plugin/swAddArticle/onAddArticle', [me, result]);

                if (buttonId == "detail-paypal"){
                    window.location.href = opts.addArticleUrl.replace("/checkout/addArticle", "/payment_paypal/express");
                    return;
                }

                /*
                if (buttonName == "listing-paypal"){
                    window.location.href = opts.addArticleUrl.replace("/checkout/addArticle", "/checkout/cart");
                    return;
                }
                */

                if (!opts.showModal) {
                    return;
                }

                $.loadingIndicator.close(function () {
                    $.modal.open(result, {
                        width: 750,
                        sizing: 'content',
                        onClose: me.onCloseModal.bind(me)
                    });

                    picturefill();

                    StateManager.updatePlugin(opts.productSliderSelector, 'swProductSlider');

                    $.publish('plugin/swAddArticle/onAddArticleOpenModal', [me, result]);
                });
            }
        });
    }

});

;(function ($, window) {
    'use strict';

    $(document).on('click', '.buybox--form button', function() {
        $('.buybox--form button').removeClass('clicked');
        $(this).addClass('clicked');
    });

})(jQuery, window);
