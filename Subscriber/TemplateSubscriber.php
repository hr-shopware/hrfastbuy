<?php

namespace HrFastBuy\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Template_Manager;
use Enlight_Controller_ActionEventArgs;

class TemplateSubscriber implements SubscriberInterface
{
    /**
     * @var Enlight_Template_Manager
     */
    private $templateManager;

    private $viewDirectory;

    /**
     * templateSubscriber constructor.
     *
     * @param Enlight_Template_Manager $templateManager
     * @param                          $viewDirectory
     */
    public function __construct(Enlight_Template_Manager $templateManager, $viewDirectory)
    {
        $this->templateManager = $templateManager;
        $this->viewDirectory   = $viewDirectory;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend'       => 'addTemplates',
            'Enlight_Controller_Action_PreDispatch_Widgets_Listing' => 'addTemplates',
        ];
    }

    public function addTemplates(Enlight_Controller_ActionEventArgs $args)
    {
        $this->templateManager->addTemplateDir($this->viewDirectory);
    }
}
